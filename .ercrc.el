;;; .ercrc.el --- config file for ERC - Emacs IRC client

;;; Commentary:
;;; My config for ERC

;;; Code:

(add-hook 'erc-mode-hook #'inhibit-whitespace-mode)

(load (custom-lisp-file "erc-highlight-nicknames"))

(setq erc-modules
      '(button
        completion
        dcc
        fill
        highlight-nicknames
        irccontrols
        keep-place
        list
        match
        menu
        move-to-prompt
        netsplit
        networks
        noncommands
        notifications
        readonly
        ring
        stamp
        spelling
        track))

(erc-update-modules)

(setq erc-interpret-mirc-color t
      erc-fill-column 100
      erc-fools '("martin-t" "malice")
      erc-hide-list '("JOIN" "PART" "QUIT")
      erc-fool-highlight-type 'all
      erc-keywords '("moros?")
      erc-fill-function 'erc-fill-static
      erc-rename-buffers t
      erc-fill-static-center 15
      erc-ignore-list nil
      erc-insert-timestamp-function #'erc-insert-timestamp-left)

;;; Had to redefine this one to get proper buffer rename
(defun erc-format-target-and/or-network ()
  "Return the network or the current target and network combined.
If the name of the network is not available, then use the
shortened server name instead."
  (let ((network-name (or (and (fboundp 'erc-network-name) (erc-network-name))
                          (erc-shorten-server-name
                           (or erc-server-announced-name
                               erc-session-server)))))
    (when (and network-name (symbolp network-name))
      (setq network-name (symbol-name network-name)))
    (cond ((erc-default-target)
           (let ((name (format "%s@%s"
                               (erc-string-no-properties (erc-default-target))
                               network-name)))
             (when erc-rename-buffers
	       (rename-buffer name))
             name))
          ((and network-name
                (not (get-buffer network-name)))
           (when erc-rename-buffers
	     (rename-buffer network-name))
           network-name)
          (t (buffer-name (current-buffer))))))

(defun erc-ctcp-query-VERSION (_proc nick _login _host _to _msg)
  "Respond to a CTCP VERSION query from NICK."
  (unless erc-disable-ctcp-replies
    (message (format "Potentially evil person `%s' has just run a CTCP VERSION query against you"
                     nick))
    (erc-send-ctcp-notice
     nick (format
           "VERSION Hello curious person! Why would you like to know which IRC client I am using?")))
  nil)

(defun erc-custom-format-nick (&optional user _channel-data)
  "Return the nickname of USER.
See also `erc-format-nick-function'."
  (when user
    (let ((name (erc-server-user-nickname user))
          (max-length (- erc-fill-static-center 5)))
      (if (> (length name) max-length)
          (concat (substring name 0 max-length) "~")
        name))))

(setq erc-format-nick-function #'erc-custom-format-nick)

(defvar erc-track-include-types '("PRIVMSG"))

(defun erc-track-modified-channels ()
  "Hook function for `erc-insert-post-hook' to check if the current
buffer should be added to the mode line as a hidden, modified
channel.  Assumes it will only be called when current-buffer
is in `erc-mode'."
  (let ((this-channel (or (erc-default-target)
			  (buffer-name (current-buffer)))))
    (if (and (not (erc-buffer-visible (current-buffer)))
	     (not (member this-channel erc-track-exclude))
	     (not (and erc-track-exclude-server-buffer
		       (erc-server-buffer-p)))
             (erc-message-type-member (or (erc-find-parsed-property)
		                          (point-min))
                                      erc-track-include-types)
	     (not (erc-message-type-member
		   (or (erc-find-parsed-property)
		       (point-min))
		   erc-track-exclude-types)))
	;; If the active buffer is not visible (not shown in a
	;; window), and not to be excluded, determine the kinds of
	;; faces used in the current message, and unless the user
	;; wants to ignore changes in certain channels where there
	;; are no faces corresponding to `erc-track-faces-priority-list',
	;; and the faces in the current message are found in said
	;; priority list, add the buffer to the erc-modified-channels-alist,
	;; if it is not already there.  If the buffer is already on the list
	;; (in the car), change its face attribute (in the cddr) if
	;; necessary.  See `erc-modified-channels-alist' for the
	;; exact data structure used.
	(let ((faces (erc-faces-in (buffer-string))))
	  (unless (and
		   (or (eq erc-track-priority-faces-only 'all)
		       (member this-channel erc-track-priority-faces-only))
		   (not (catch 'found
			  (dolist (f faces)
			    (when (member f erc-track-faces-priority-list)
			      (throw 'found t))))))
	    (if (not (assq (current-buffer) erc-modified-channels-alist))
		;; Add buffer, faces and counts
		(setq erc-modified-channels-alist
		      (cons (cons (current-buffer)
				  (cons 1 (erc-track-find-face faces)))
			    erc-modified-channels-alist))
	      ;; Else modify the face for the buffer, if necessary.
	      (when faces
		(let* ((cell (assq (current-buffer)
				   erc-modified-channels-alist))
		       (old-face (cddr cell))
		       (new-face (erc-track-find-face
				  (if old-face
				      (cons old-face faces)
				    faces))))
		  (setcdr cell (cons (1+ (cadr cell)) new-face)))))
	    ;; And display it
	    (erc-modified-channels-display)))
      ;; Else if the active buffer is the current buffer, remove it
      ;; from our list.
      (when (and (or (erc-buffer-visible (current-buffer))
		(and this-channel
		     (member this-channel erc-track-exclude)))
		 (assq (current-buffer) erc-modified-channels-alist))
	;; Remove it from mode-line if buffer is visible or
	;; channel was added to erc-track-exclude recently.
	(erc-modified-channels-remove-buffer (current-buffer))
	(erc-modified-channels-display)))))


;;; .ercrc.el ends here
