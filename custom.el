(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-safe-themes
   '("ed35894cb3637c754cf6a9cab2ec1eca525452894eeedf46844a8e3dc7e1089e" "b15473f0bb1d124cd02f356715b63e1af8386f1c93d1bf8b0b315b04d1725225" "a6a75d921a28088484a059d64220f3d3c41f6bbbfa91fe154cbe9019256023a3" "5b25fa35523f4958e6086bd823b7eb9c44aae4bc9c99d8b265fe11205df27779" "594df9401f0f37916120d7afe00bd7cc80025eb548cf1e19075ce3845b2f84ae" "41f7f823fcb006096dfd436b91b9fb680a94b0a181cfff2767e1439123a51fbf" "d5d87452e2db156d10019017b744dfcc0cd40deae50817e95b8d1b797221468d" "6dc21192d4020ce80369d260c4dab773cdb449a5aa2d63f63093860f6627caed" "fc86631a52d5a24de87f2accdbcedd59c7e9ac8984c29ec12111d770a1b8cbfc" "451e57275e15f2d2aed5301e9cefd73cdd22bcbebfbcc5bd5d6ff7735307394f" "6efccdc507cb790d4f0aadd8dca7f260f0125c22da1b37518f577c53c11d0a3c" "0f2f1feff73a80556c8c228396d76c1a0342eb4eefd00f881b91e26a14c5b62a" "82d2cac368ccdec2fcc7573f24c3f79654b78bf133096f9b40c20d97ec1d8016" "628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "bb08c73af94ee74453c90422485b29e5643b73b05e8de029a6909af6a3fb3f58" "7e22a8dcf2adcd8b330eab2ed6023fa20ba3b17704d4b186fa9c53f1fab3d4d2" "7b3ce93a17ce4fc6389bba8ecb9fee9a1e4e01027a5f3532cc47d160fe303d5a" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "dbade2e946597b9cda3e61978b5fcc14fa3afa2d3c4391d477bdaeff8f5638c5" "801a567c87755fe65d0484cb2bded31a4c5bb24fd1fe0ed11e6c02254017acb2" "d4131a682c4436bb5a61103d9a850bf788cbf793f3fd8897de520d20583aeb58" default))
 '(doom-modeline-mode nil)
 '(ecb-options-version "2.50")
 '(git-gutter:added-sign "++")
 '(git-gutter:deleted-sign "--")
 '(git-gutter:modified-sign "==")
 '(git-gutter:separator-sign nil)
 '(gud-gdb-command-name "gdb --annotate=1")
 '(indent-tabs-mode nil)
 '(large-file-warning-threshold nil)
 '(line-number-mode t)
 '(org-agenda-files '("~/org/habits.org" "~/org/xonotic.org"))
 '(org-modules
   '(ol-bbdb ol-bibtex ol-docview ol-eww ol-gnus org-habit ol-info ol-irc ol-mhe ol-rmail ol-w3m))
 '(package-selected-packages
   '(frames-only-mode lua-mode base16-theme evil-cleverparens lush-theme exec-path-from-shell tree-sitter evil-surround spaceline evil-visual-mark-mode evil-textobj-line evil-nerd-commenter evil-collection evil-matchit evil-goggles evil-giggles evil-tutor evil-mode meow evil go-mode nim-mode fish-mode tree-sitter-langs olivetti org-plus-contrib projectile pyvenv solarized-theme leuven-theme borland-blue-theme dap-mode inf-clojure cider clojure-mode eshell-prompt-extras consult boon dimmer crontab-mode orderless marginalia vertico dired-open alect-themes notink-theme pass dired-quick-sort bufler winner-mode ranger dired-launch dired-launcher diredc super-save dired elcord lsp-treemacs zoxide org-roam vlf company-capf fancy-battery awesome-tray material-theme nov auto-dim-other-buffers powerline perspective color-identifiers-mode winum company-lsp git-gutter pip-requirements golden-ratio volatile-highlights aggressive-indent rainbow-delimiters mini-modeline smart-mode-line mini-frame emacs-mini-frame ctrlf tao-theme magit-lfs visual-fill-column org-mode rg prescient js2-mode docker dhall-mode yapfify company-anaconda anaconda-mode ox ox-hugo vterm haskell-mode systemd php-mode company-nginx nginx-mode pdf-tools smartparens expand-region erc-status-sidebar emojify rustic-mode dired-git-info diredfl howdoyou notmuch elscreen-fr diminish god-mode treemacs-magit treemacs wgrep lsp-ui diff-hl elscreen emms whitespace-cleanup-mode ace-window helpful which-key all-the-icons-ivy all-the-icons-dired all-the-icons deadgrep elpy magit-todos flycheck-ledger ledger-mode mu4e-alert magit yaml-mode lsp-mode use-package xcscope flycheck dockerfile-mode toml-mode mmm-mode json-mode csv-mode dotenv-mode flymake-shellcheck rustic cargo flycheck-rust flymake-rust lsp-rust racer rust-auto-use))
 '(safe-local-variable-values
   '((rustic-format-on-save)
     (rustic-format-on-save . t)
     (rustic-format-on-save nil)
     (rustic-format-on-save t)
     (hamlet/basic-offset . 4)
     (haskell-process-use-ghci . t)
     (haskell-indent-spaces . 4)))
 '(send-mail-function 'mailclient-send-it)
 '(size-indication-mode t)
 '(warning-suppress-log-types '((lsp-mode)))
 '(warning-suppress-types '((comp)))
 '(which-key-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight semi-bold :height 110 :width normal :foundry "TAB " :family "Comic Code Ligatures"))))
 '(erc-input-face ((t nil)))
 '(erc-my-nick-face ((t (:foreground "#b8bb26"))))
 '(erc-timestamp-face ((t (:foreground "#a89984"))))
 '(fixed-pitch ((t nil)))
 '(fixed-pitch-serif ((t nil))))
