(setq org-publish-project-alist
      '(("writes"
         :base-directory "~/writes"
         :publishing-directory "/tmp/writes"
         :publishing-function org-html-publish-to-html)))
