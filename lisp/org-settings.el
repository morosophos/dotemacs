(require 'org)

(setq org-default-notes-file (concat org-directory "/capture.org"))
(setq org-use-speed-commands t)

(global-set-key (kbd "<f5>") 'org-capture)

(defun local-keybindings ()
  (define-key org-mode-map (kbd "C-<right>") 'org-shiftright)
  (define-key org-mode-map (kbd "C-<left>") 'org-shiftleft)
  (define-key org-mode-map (kbd "C-<up>") 'org-shiftup)
  (define-key org-mode-map (kbd "C-<down>") 'org-shiftdown)
  (define-key org-mode-map (kbd "S-<left>") nil)
  (define-key org-mode-map (kbd "S-<right>") nil)
  (define-key org-mode-map (kbd "S-<up>") nil)
  (define-key org-mode-map (kbd "S-<down>") nil))


(add-hook 'org-mode-hook 'local-keybindings)

(provide 'org-settings)
