(setq gnus-select-method
      '(nnmaildir "mail" (directory "~/mail/teichisma")))

(setq
   message-send-mail-function   'smtpmail-send-it
   smtpmail-default-smtp-server "mail.teichisma.info"
   smtpmail-smtp-server         "mail.teichisma.info"
   smtpmail-smtp-service 587
   smtpmail-starttls-credentials '(("mail.teichisma.info" 587 nil nil))
   smtpmail-auth-credentials '(("mail.teichisma.info" 587
				"morosophos@teichisma.info" nil)))

(setq gnus-subscribe-newsgroup-method 'gnus-subscribe-randomly)

(setq gnus-thread-sort-functions
      '(gnus-thread-sort-by-most-recent-number))

(setq gnus-article-sort-functions
      '(gnus-thread-sort-by-most-recent-date))

(setq gnus-summary-thread-gathering-function 'gnus-gather-threads-by-references)
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

(gnus-demon-add-handler 'gnus-demon-scan-news 2 nil)
(gnus-demon-init)

(setq gnus-visual '(highlight menu))
