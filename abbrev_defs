;;-*-coding: utf-8;-*-
(define-abbrev-table 'rst-mode-abbrev-table
  '(
    ("con" ".. contents::
..
   " nil :count 0)
    ("cont" "[...]" nil :count 0)
    ("contents" ".. contents::
..
   " nil :count 0)
    ("seq" "

[...]

  " nil :count 0)
    ("skip" "

[...]

  " nil :count 0)
   ))

