;;; init --- emacs configuration file -*- lexical-binding: t -*-

;;; Commentary:
;;; Emacs configuration file

;;; Code:

;;; distracting visuals
(menu-bar-mode 0)

(setq initial-buffer-choice "~/writes/entrypoint.org")
;; (add-to-list 'default-frame-alist '(alpha 98 98))

(add-hook 'server-after-make-frame-hook
          (lambda ()
            (interactive)
            (tool-bar-mode -1)
            (scroll-bar-mode -1)
            (column-number-mode 1)
            (line-number-mode 1)
            (show-paren-mode 1)
            (size-indication-mode -1)))

(setq show-paren-highlight-openparen t
      show-paren-when-point-inside-paren t
      show-paren-when-point-in-periphery t
      show-paren-context-when-offscreen t)

(setq inhibit-startup-screen t)
(setq ring-bell-function (lambda () (message "*bell rings*")))
(setq save-interprogram-paste-before-kill t)

(setq fill-column 100)
(blink-cursor-mode 1)
(setq blink-cursor-interval 0.8
      blink-cursor-delay 0.5)
(global-display-line-numbers-mode +1)

(setq mac-command-modifier 'meta
      mac-option-modifier 'control)

(setq user-full-name "Nick S."
      user-mail-address "nick@teichisma.info")

(setq-default cursor-type 'bar)

(setq frame-title-format "EMACS"
      frame-resize-pixelwise t)

(defun custom-lisp-file (path)
  "Return path to an element in Lisp code collection.
PATH is the name of the element.  The collection has been greatly reduced
since MELPA was introduced."
  (expand-file-name (concat "lisp/" path) user-emacs-directory))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

(add-to-list 'load-path (custom-lisp-file ""))
(setq fortune-file "/usr/share/fortune/bible")

;; (load (custom-lisp-file "russian-colemakdh"))
;; (setq default-input-method "cyrillic-colemakdh")

(setq vc-handled-backends '(Git))
(let ((d (expand-file-name "tramp-autosaves" user-emacs-directory)))
  ;; (make-directory d)
  (setq tramp-auto-save-directory d))

(defun electric-beginning-of-line (&optional n)
  "Move the point to the beginning of text unless the point is already there.
Move to the beginning of line otherwise.
If N is given, move forward N-1 lines first."
  (interactive "p")
  (if (> n 1)
      (beginning-of-line-text n)
    (let ((prev-point (point)))
      (beginning-of-line-text)
      (when (eq prev-point (point))
        (beginning-of-line)))))

(defun forward-char-within-current-line (&optional n)
  "Move the point N characters forward but no further than the end of the current line."
  (interactive "p")
  (let ((line-end (- (line-end-position) (point))))
    (forward-char (min line-end n))))

(defun backward-char-within-current-line (&optional n)
  "Move the point N characters backward but no further than the beginning of the current line."
  (interactive "p")
  (let ((line-start (- (point) (line-beginning-position))))
    (backward-char (min line-start n))))

(defun split-window-below-and-switch (&optional size)
  "Do `split-window-below' then switch to the newly created window.
Optional SIZE is the size of the new window."
  (interactive "P")
  (split-window-below size)
  (other-window 1))

(defun split-window-right-and-switch (&optional size)
  "Do `split-window-right' then switch to the newly created window.
Optional SIZE is the size of the new window."
  (interactive "P")
  (split-window-right size)
  (other-window 1))

(defun save-line-to-killring ()
  (interactive)
  (let ((beg (point))
        (end (save-excursion
               (end-of-visible-line)
               (point))))
    (kill-ring-save beg end)))

(global-set-key (kbd "C-S-K") #'save-line-to-killring)

;;; Global movement keys
(dolist (keydef '(("C-b" . backward-char-within-current-line)
                  ("C-f" . forward-char-within-current-line)
                  ("C-a" . electric-beginning-of-line)
                  ("<home>" . beginning-of-line)
                  ("<end>" . end-of-line)
                  ("C-<" . beginning-of-buffer)
                  ("C->" . end-of-buffer)))
  (global-set-key (kbd (car keydef)) (cdr keydef)))
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x C-z"))
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "TAB") #'indent-for-tab-command)
(global-set-key (kbd "C-x C-1") 'delete-other-windows)
(global-set-key (kbd "C-x C-2") 'split-window-below-and-switch)
(global-set-key (kbd "C-x C-3") 'split-window-right-and-switch)
(global-set-key (kbd "C-x C-0") 'delete-window)
(global-set-key (kbd "C-x C-k") 'kill-buffer)
(global-set-key (kbd "C-x C-b") 'switch-to-buffer)

(setq kill-whole-line t)

;; (load (custom-lisp-file "russian-dvorak.el"))
;; (set-input-method 'cyrillic-dvorak)
;; (toggle-input-method)

(add-to-list 'auto-mode-alist '("\\.jinja\\'" . html-mode))

(setq set-mark-command-repeat-pop t
      backup-inhibited t)

(defalias 'yes-or-no-p 'y-or-n-p)

(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

(require 'winner)
(winner-mode 1)

(transient-mark-mode 1)

(defun greek-colon ()
  "Insert greek colon character."
  (interactive)
  (insert-string "·"))

(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'set-goal-column 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(dolist (ep '("~/.local/bin"
              "~/.cargo/bin"
              "~/.nimble/bin"
              "~/go/bin"))
  (add-to-list 'exec-path (expand-file-name ep)))

(recentf-mode 1)
(global-auto-revert-mode t)
(setq global-auto-revert-non-file-buffers t)

(setq shr-use-colors nil)

(setq
   message-send-mail-function   'smtpmail-send-it
   smtpmail-default-smtp-server "mail.teichisma.info"
   smtpmail-smtp-server         "mail.teichisma.info"
   smtpmail-smtp-service 587
   smtpmail-starttls-credentials '(("mail.teichisma.info" 587 nil nil))
   smtpmail-auth-credentials '(("mail.teichisma.info" 587
				"morosophos@teichisma.info" nil)))


(setq ispell-dictionary "en_US")
(setq ispell-alternate-dictionary "ru_RU")

(save-place-mode 1)


(setq dired-listing-switches "-algho --group-directories-first"
      dired-dwim-target t)

;;; Packages
(defvar bootstrap-version)

(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-use-package-by-default t)
(straight-use-package 'use-package)

(setq package-enable-at-startup nil)

(require 'use-package-ensure)

(use-package diminish
  :init
  (diminish 'eldoc-mode))

;; (use-package base16-theme
;;   :init
;;   (setq base16-theme-distinct-fringe-background nil)
;;   (defvar my-base16-theme)
;;   (setq my-base16-theme 'base16-twilight)
;;   (load-theme my-base16-theme t))

(use-package god-mode
  :init (god-mode)
  (defun my-god-mode-update-cursor-type ()
    (setq cursor-type (if (or god-local-mode buffer-read-only) 'box 'bar)))
  ;; (require 'god-mode-isearch)
  ;; (define-key isearch-mode-map (kbd "<escape>") #'god-mode-isearch-activate)
  ;; (define-key god-mode-isearch-map (kbd "C-i") #'god-mode-isearch-disable)
  (add-hook 'post-command-hook #'my-god-mode-update-cursor-type)
  (global-set-key (kbd "C-t") (lambda () (interactive) (god-local-mode -1)))
  (global-set-key (kbd "<escape>") (lambda () (interactive) (god-local-mode 1))))

;; (use-package evil
;;   :init
;;   (setq evil-undo-system 'undo-redo)
;;   (setq evil-want-integration t)
;;   (setq evil-want-keybinding nil)
;;   (evil-mode 1)
;;   :config

;;   (add-hook 'edebug-mode-hook 'evil-normalize-keymaps)
;;   (define-key evil-outer-text-objects-map "d" 'my-inner-defun)
;;   (define-key evil-inner-text-objects-map "d" 'my-inner-defun)
;;   ;; (defvar evil-cursor-base16-colors)
;;   ;; (setq evil-cursor-base16-colors (eval (intern (format "%s-theme-colors" my-base16-theme))))
;;   ;; (setq evil-emacs-state-cursor   `(,(plist-get evil-cursor-base16-colors :base0D) box)
;;   ;;       evil-insert-state-cursor  `(,(plist-get evil-cursor-base16-colors :base0D) bar)
;;   ;;       evil-motion-state-cursor  `(,(plist-get evil-cursor-base16-colors :base0E) box)
;;   ;;       evil-normal-state-cursor  `(,(plist-get evil-cursor-base16-colors :base0B) box)
;;   ;;       evil-replace-state-cursor `(,(plist-get evil-cursor-base16-colors :base08) bar)
;;   ;;       evil-visual-state-cursor  `(,(plist-get evil-cursor-base16-colors :base09) box))
;;   (evil-define-key '(normal visual motion) 'global (kbd "C-n") #'avy-goto-char-timer))

;; (use-package evil-surround
;;   :config
;;   (global-evil-surround-mode 1))

;; (use-package evil-visual-mark-mode
;;   :config (evil-visual-mark-mode +1))

;; (use-package evil-goggles
;;   :config (evil-goggles-mode +1))

;; (use-package evil-matchit
;;   :config (global-evil-matchit-mode +1))

;; (use-package evil-nerd-commenter
;;   :config (evilnc-default-hotkeys))

;; (use-package evil-textobj-line)

;; (use-package evil-collection
;;   :config
  ;; (setq evil-collection-mode-list '(alchemist anaconda-mode apropos arc-mode atomic-chrome auto-package-update beginend bm bookmark
  ;;                                             (buff-menu "buff-menu")
  ;;                                             calc calendar cider cmake-mode comint company compile consult
  ;;                                             (custom cus-edit)
  ;;                                             cus-theme dashboard daemons deadgrep debbugs debug devdocs dictionary diff-hl diff-mode dired dired-sidebar disk-usage distel doc-view docker ebib ebuku edbi edebug ediff eglot explain-pause-mode eldoc elfeed elisp-mode elisp-refs elisp-slime-nav embark emms emoji epa ert eshell eval-sexp-fu evil-mc eww fanyi finder flycheck flymake forge free-keys geiser ggtags git-timemachine gnus go-mode grep guix hackernews helm help helpful hg-histedit hungry-delete ibuffer image image-dired image+ imenu imenu-list
  ;;                                             (indent "indent")
  ;;                                             indium info ivy js2-mode leetcode lispy log-edit log-view lsp-ui-imenu lua-mode kotlin-mode macrostep man
  ;;                                             markdown-mode monky mpc mpdel neotree newsticker notmuch nov
  ;;                                             org org-present org-roam osx-dictionary outline p4
  ;;                                             (package-menu package)
  ;;                                             pass
  ;;                                             (pdf pdf-view)
  ;;                                             popup proced
  ;;                                             (process-menu simple)
  ;;                                             prodigy profiler python quickrun racer racket-describe realgud reftex restclient rg ripgrep rjsx-mode robe rtags ruby-mode scheme scroll-lock selectrum sh-script shortdoc simple simple-mpc slime sly speedbar tab-bar tablist tabulated-list tar-mode telega
  ;;                                             (term term ansi-term multi-term)
  ;;                                             tetris thread tide timer-list transmission trashed tuareg typescript-mode vc-annotate vc-dir vc-git vdiff vertico view vlf vterm vundo w3m wdired wgrep which-key woman xref xwidget yaml-mode youtube-dl zmusic
  ;;                                             (ztree ztree-diff ztree-dir)))
 ;; (defun evil-hjkl-rotation (mode mode-keymaps &rest _rest)
 ;;    (evil-collection-translate-key mode mode-keymaps
 ;;      "l" "u"
 ;;      "m" "h"
 ;;      "h" "m"
 ;;      "n" "j"
 ;;      "e" "k"
 ;;      "ge" "gk"
 ;;      "i" "l"
 ;;      "j" "e"
 ;;      "k" "n"
 ;;      "gk" "gn"
 ;;      "u" "i"
 ;;      "L" "U"
 ;;      "M" "H"
 ;;      "H" "M"
 ;;      "N" "J"
 ;;      "E" "K"
 ;;      "ge" "gK"
 ;;      "I" "L"
 ;;      "J" "E"
 ;;      "K" "N"
 ;;      "gK" "gN"
 ;;      "U" "I"))
 ;; ;; called after evil-collection makes its keybindings
 ;;  ;; (add-hook 'evil-collection-setup-hook
 ;;  ;;           (lambda (_mode mode-keymaps &rest _rest)
 ;;  ;;             (evil-hjkl-rotation '(visual normal motion) mode-keymaps)))
 ;;  (evil-collection-init)
 ;;  (diminish 'evil-collection-unimpaired-mode)
 ;;  ;; (evil-hjkl-rotation nil 'evil-motion-state-map)
 ;;  ;; (evil-hjkl-rotation nil 'evil-normal-state-map)
 ;;  ;; (evil-hjkl-rotation nil 'evil-visual-state-map)
 ;;  )

(use-package paredit)

;; (use-package evil-cleverparens
;;   :straight (evil-cleverparens-mode :type git :host github :repo "luxbock/evil-cleverparens"
;;                                     :fork (:host github
;;                                                  :repo "nsavch/evil-cleverparens"))
;;   :init
  ;; (setq evil-cp-insert-key "u"
  ;;       evil-cp-append-key "a")
  ;; (setq evil-cp-regular-movement-keys
  ;;       '(("w"  . evil-forward-word-begin)
  ;;         ("j"  . evil-forward-word-end)
  ;;         ("b"  . evil-backward-word-begin)
  ;;         ("gj" . evil-backward-word-end)
  ;;         ("W"  . evil-cp-forward-symbol-begin)
  ;;         ("J"  . evil-cp-forward-symbol-end)
  ;;         ("B"  . evil-cp-backward-symbol-begin)
  ;;         ("gJ" . evil-cp-backward-symbol-end)))

  ;; (setq evil-cp-swapped-movement-keys
  ;;       '(("w"  . evil-cp-forward-symbol-begin)
  ;;         ("j"  . evil-cp-forward-symbol-end)
  ;;         ("b"  . evil-cp-backward-symbol-begin)
  ;;         ("gj" . evil-cp-backward-symbol-end)
  ;;         ("W"  . evil-forward-word-begin)
  ;;         ("J"  . evil-forward-word-end)
  ;;         ("B"  . evil-backward-word-begin)
  ;;         ("gJ" . evil-backward-word-end)))

  ;; (setq evil-cp-additional-movement-keys
  ;;       '(("I"   . evil-cp-forward-sexp)
  ;;         ("M"   . evil-cp-backward-sexp)
  ;;         ("M-i" . evil-cp-end-of-defun)
  ;;         ("M-m" . evil-cp-beginning-of-defun)
  ;;         ("["   . evil-cp-previous-opening)
  ;;         ("]"   . evil-cp-next-closing)
  ;;         ("{"   . evil-cp-next-opening)
  ;;         ("}"   . evil-cp-previous-closing)
  ;;         ("("   . evil-cp-backward-up-sexp)
  ;;         (")"   . evil-cp-up-sexp)))

  ;; (setq evil-cp-regular-bindings
  ;;       '(("d"   . evil-cp-delete)
  ;;         ("c"   . evil-cp-change)
  ;;         ("y"   . evil-cp-yank)
  ;;         ("D"   . evil-cp-delete-line)
  ;;         ("C"   . evil-cp-change-line)
  ;;         ("Y"   . evil-cp-yank-line)
  ;;         ("x"   . evil-cp-delete-char-or-splice)
  ;;         ("X"   . evil-cp-delete-char-or-splice-backwards)
  ;;         (">"   . evil-cp->)
  ;;         ("<"   . evil-cp-<)
  ;;         ("_"   . evil-cp-first-non-blank-non-opening)
  ;;         ("M-T" . evil-cp-toggle-balanced-yank)
  ;;         ("M-z" . evil-cp-override)))

  ;; (setq evil-cp-additional-bindings
  ;;       '(("M-t" . sp-transpose-sexp)
  ;;         ("M-e" . evil-cp-drag-backward)
  ;;         ("M-n" . evil-cp-drag-forward)
  ;;         ("M-N" . sp-join-sexp)
  ;;         ("M-s" . sp-splice-sexp)
  ;;         ("M-S" . sp-split-sexp)
  ;;         ("M-R" . evil-cp-raise-form)
  ;;         ("M-r" . sp-raise-sexp)
  ;;         ("M-a" . evil-cp-insert-at-end-of-form)
  ;;         ("M-u" . evil-cp-insert-at-beginning-of-form)
  ;;         ("M-w" . evil-cp-copy-paste-form)
  ;;         ("M-y" . evil-cp-yank-sexp)
  ;;         ("M-d" . evil-cp-delete-sexp)
  ;;         ("M-c" . evil-cp-change-sexp)
  ;;         ("M-Y" . evil-cp-yank-enclosing)
  ;;         ("M-D" . evil-cp-delete-enclosing)
  ;;         ("M-C" . evil-cp-change-enclosing)
  ;;         ("M-q" . sp-indent-defun)
  ;;         ("o" . evil-cp-open-below-form)
  ;;         ("O" . evil-cp-open-above-form)
  ;;         ("M-v" . sp-convolute-sexp)
  ;;         ("M-(" . evil-cp-wrap-next-round)
  ;;         ("M-)" . evil-cp-wrap-previous-round)
  ;;         ("M-[" . evil-cp-wrap-next-square)
  ;;         ("M-]" . evil-cp-wrap-previous-square)
  ;;         ("M-{" . evil-cp-wrap-next-curly)
  ;;         ("M-}" . evil-cp-wrap-previous-curly)))

  ;; (setq evil-cleverparens-swap-move-by-word-and-symbol t)
  ;; (setq evil-cleverparens-use-additional-movement-keys t)
  ;; (setq evil-cleverparens-use-additional-bindings t)
  ;; (require 'evil-cleverparens-text-objects)
  ;; (add-hook 'lisp-mode-hook #'evil-cleverparens-mode)
  ;; (add-hook 'emacs-lisp-mode-hook #'evil-cleverparens-mode)
  ;; (add-hook 'clojure-mode-hook #'evil-cleverparens-mode))

;; (use-package evil-smartparens
;;   :hook (smartparens-enabled-hook . #'evil-smartparens-mode))

(use-package cc-mode
  :init (define-derived-mode
          qc-mode
          c-mode
          "QuakeC"
          "Major mode for editing QuakeC files"
          :after-hook (progn
                        (flycheck-mode -1)))
  :mode (("\\.qc\\'" . qc-mode)
         ("\\.qh\\'" . qc-mode)))

(use-package smartparens
  :init
  (require 'smartparens-config)
  (require 'smartparens-rust)
  (defvar smartparens-ext-map (make-sparse-keymap))
  (sp-with-modes '(rust-mode rustic-mode)
    (sp-local-pair "'" "'"
                   :unless '(sp-in-comment-p sp-in-string-quotes-p sp-in-rust-lifetime-context)
                   :post-handlers'(:rem sp-escape-quotes-after-insert))
    (sp-local-pair "<" ">"
                   :when '(sp-rust-filter-angle-brackets)
                   :skip-match 'sp-rust-skip-match-angle-bracket)
    (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET"))))
  (dolist (hook '(emacs-lisp-mode-hook clojure-mode-hook lisp-mode-hook))
    (add-hook hook #'smartparens-strict-mode)))

(use-package expand-region
  :bind ("C-M-<SPC>" . 'er/expand-region))

(use-package lsp-mode
  :init
  (setq lsp-rust-server 'rust-analyzer)
  ;; (setq lsp-rust-server 'lsp)
  (setq lsp-rust-analyzer-cargo-watch-command "clippy")
  (setq lsp-file-watch-threshold 10000)
  (setq lsp-rust-analyzer-cargo-run-build-scripts t)
  (setq lsp-rust-analyzer-proc-macro-enable t)
  (setq lsp-enable-snippet t)
  :hook ((rustic-mode . lsp)
         (lsp-mode . lsp-enable-which-key-integration)))

(use-package lsp-ui :commands lsp-ui-mode
  :config (lsp-ui-doc-mode -1))

(use-package dap-mode
  :ensure
  :config
  (dap-ui-mode)
  (dap-ui-controls-mode 1)

  (require 'dap-lldb)
  (require 'dap-gdb-lldb)
  ;; installs .extension/vscode
  (dap-gdb-lldb-setup)
  (dap-register-debug-template
   "Rust::LLDB Run Configuration"
   (list :type "lldb"
         :request "launch"
         :name "LLDB::Run"
	 :gdbpath "rust-lldb"
         :target nil
         :cwd nil)))

(use-package rustic
  :init
  (require 'lsp-rust)
  (setq rustic-lsp-client 'lsp-mode)
  (setq rustic-lsp-setup-p nil)
  (setq rustic-format-on-save nil)
  ;; (add-hook 'eglot--managed-mode-hook (lambda () (flymake-mode -1)))
  (add-hook 'rustic-mode-hook (lambda ()
                                (smartparens-mode +1)
                                (push 'rustic-clippy flycheck-checkers))))

(use-package helpful
  :bind (("C-h f" . 'helpful-callable)
         ("C-h v" . 'helpful-variable)
         ("C-h k" . 'helpful-key)
         ("C-h C-f" . 'helpful-callable)
         ("C-h C-v" . 'helpful-variable)
         ("C-h C-k" . 'helpful-key)))

(use-package savehist
  :init
  (savehist-mode 1))

(use-package whitespace
  :init
  (setq whitespace-style '(face trailing empty))
  (setq whitespace-action nil)
  (global-whitespace-mode t)
  :diminish global-whitespace-mode
  :config
  (defun check-trailing-whitespace ()
    (when buffer-file-name
      (unless (eq (char-after (1- (point-max))) 10)
        (save-excursion
          (goto-char (point-max))
          (insert "\n")))))
  (defun inhibit-whitespace-mode ()
    "Disable whitespace mode"
    (interactive)
    (whitespace-mode -1))
  (add-hook 'write-file-functions 'check-trailing-whitespace))

(use-package magit
  :init
  (global-set-key (kbd "C-x g") 'magit-status)
  (setq global-magit-file-mode t)
  (add-hook 'after-save-hook 'magit-after-save-refresh-status t))

(use-package magit-lfs)

(use-package magit-todos)

(use-package vertico
  :init
  (vertico-mode)
  :config
  (define-key vertico-map "?" #'minibuffer-completion-help)
  (define-key vertico-map (kbd "M-RET") #'minibuffer-force-complete-and-exit)
  (define-key vertico-map (kbd "M-TAB") #'minibuffer-complete))

(use-package marginalia
  :init
  (marginalia-mode)
  :bind(
        :map minibuffer-local-map
        ("M-A" . marginalia-cycle)))

(use-package orderless
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

(use-package rg
  :init (rg-enable-default-bindings))

(use-package ace-window
  :bind
  ("M-o" . 'ace-window))

(use-package all-the-icons)

(use-package all-the-icons-dired
  :init (add-hook 'dired-mode-hook 'all-the-icons-dired-mode))

(use-package which-key
  :init
  (which-key-mode t)
  :diminish which-key-mode)

;; (use-package erc
;;   :init
;;   ;;; Why do I have to require it here? :(
;;   (require 'erc)
;;   (defun erc-connect-quakenet ()
;;     (interactive)
;;     (erc-tls
;;      :server "znc.teichisma.info"
;;      :port 10621
;;      :nick "morosophos"
;;      :full-name "noname"))

;;   (defun erc-connect-freenode ()
;;     (interactive)
;;     (erc-tls
;;      :server "znc2.teichisma.info"
;;      :port 10621
;;      :nick "morosophos"
;;      :full-name "noname")))


(use-package dired-open
  :config
  (add-to-list 'dired-open-functions #'dired-open-xdg t))

(use-package dired-quick-sort
  :init (dired-quick-sort-setup))

(use-package diredfl
  :config
  (diredfl-global-mode 1))

(use-package dired-single
  :bind (:map dired-mode-map
              ([return] . 'dired-single-buffer)
              ([mouse-1] . 'dired-single-buffer-mouse)
              ("^" . 'dired-single-up-directory)))

(use-package company
  :init
  (global-company-mode t)
  :diminish company-mode
  :config
  (setq company-idle-delay 0.5
        company-show-numbers t))

;; (use-package perspective
;;   :init
;;   (setq persp-initial-frame-name "!main")
;;   (setq persp-suppress-no-prefix-key-warning t)
;;   (persp-mode)
;;   (setq persp-state-default-file (expand-file-name "persp.state" user-emacs-directory))
;;   (evil-define-key '(visual normal motion insert) 'global (kbd "C-z") perspective-map)
;;   :bind (("C-x b" . persp-switch-to-buffer*)
;;          ("C-x k" . persp-kill-buffer*))
;;   :config
;;   (add-hook 'persp-created-hook (lambda (&rest _args)
;;                                  (find-file initial-buffer-choice)))
;;   (defun switch-to-nth-perspective (n)
;;     (let ((name (nth (- n 1) (persp-names))))
;;       (persp-switch name)))
;;   (dolist (i '(1 2 3 4 5 6 7 8 9 0))
;;     (define-key
;;       perspective-map
;;       (format "%d" i)
;;       (lambda () (interactive) (switch-to-nth-perspective i))))
;;   (unless noninteractive
;;     (add-hook 'kill-emacs-hook #'persp-state-save))
;;   ;; (setq display-buffer-alist
;;   ;;       '((".*" (display-buffer-reuse-window display-buffer-same-window))))
;;   ;; (setq display-buffer-reuse-frames t)
;;   (setq even-window-sizes nil))

(use-package flymake
  :config
  (remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake))

(use-package yasnippet
  :init
  (yas-global-mode 1))
(diminish 'yas-minor-mode)

(use-package yaml-mode)
(use-package wgrep)
(use-package toml-mode)

(use-package ledger-mode)
(use-package json-mode)
(use-package js2-mode
  :init
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode)))

;; (use-package lush-theme
;;   :init (load-theme 'lush t))

(use-package gruvbox-theme
  :init
  (load-theme 'gruvbox-dark-hard t))

;; (use-package color-theme-sanityinc-tomorrow
;;   :init
;;   (color-theme-sanityinc-tomorrow-night))

;; (use-package quasi-monochrome-theme
;;   :init
;;   (load-theme 'quasi-monochrome t))

;; (use-package material-theme
;;   :init
;;   (setq custom-safe-themes t)
;;   (load-theme 'material))

;; (use-package modus-themes
;;   :init
;;   ;; NOTE: Everything is disabled by default.
;;   (setq
;;         modus-themes-3d-modeline nil
;;         modus-themes-bold-constructs t
;;         modus-themes-completions '((matches . (extrabold)) (selection . (semibold accented)) (popup . (accented intense)))
;;         modus-themes-diffs 'desaturated ; {nil,'desaturated,'fg-only}
;;         modus-themes-fringes 'subtle ; {nil,'subtle,'intense}
;;         modus-themes-intense-hl-line nil
;;         modus-themes-org-blocks 'rainbow ; {nil,'greyscale,'rainbow}
;;         modus-themes-paren-match '(intense)
;;         modus-themes-prompts '(intense bold) ; {nil,'subtle,'intense}
;;         modus-themes-rainbow-headings t
;;         modus-themes-scale-headings t
;;         modus-themes-section-headings t
;;         modus-themes-slanted-constructs t
;;         modus-themes-subtle-line-numbers nil
;;         modus-themes-variable-pitch-headings t
;;         )
;;   :config
;;   (load-theme 'modus-vivendi t))

;; (use-package borland-blue-theme
;;   :init
;;   (load-theme 'borland-blue t))

(use-package flymake-shellcheck
  :init
  (add-hook 'sh-mode-hook 'flymake-shellcheck-load))

(use-package flycheck
  :init
  (global-flycheck-mode t)
  :config
  (setq flycheck-checker-error-threshold 10000))

(use-package flycheck-ledger)
(use-package flycheck-rust)

;; (use-package emms
;;   :config
;;   (require 'emms-setup)
;;   (emms-all)
;;   (emms-default-players)
;;   (setq emms-source-file-default-directory "/storage/nick/music"))

(use-package dockerfile-mode
  :mode ("Dockerfile.*" . dockerfile-mode))

(use-package dotenv-mode)

(use-package pdf-tools)

(use-package nginx-mode)

(use-package company-nginx
  :config
  (eval-after-load 'nginx-mode
    '(add-hook 'nginx-mode-hook #'company-nginx-keywords)))

(use-package php-mode)

(use-package systemd)

(use-package haskell-mode)

(eval-when-compile
  (when (and (not (eq system-type 'darwin)) (display-graphic-p))
    (use-package vterm)))

;;; python
(use-package anaconda-mode
  :init
  (add-to-list 'flycheck-disabled-checkers 'python-flake8)
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode))

(use-package company-anaconda)
(use-package pyvenv)

(use-package projectile
  :init
  (projectile-mode 1)
  (diminish 'projectile-mode)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (setq projectile-completion-system 'default))

(use-package dhall-mode)

(use-package docker
  :bind ("C-c d" . docker))

;; (use-package yapfify
;;   :init
;;   (add-hook 'python-mode-hook 'yapf-mode))

(use-package org
  :bind (("C-c l" . org-store-link)
         ("C-c a" . org-agenda)
         ("C-c c" . org-capture))
  :init (setq-default major-mode 'org-mode)
  :hook
  ((org-mode) . (lambda ()
                  (interactive)
                  (org-indent-mode +1)
                  (electric-indent-mode -1)
                  (auto-fill-mode +1)
                  (setq-local fill-column 96)))
  :config
  (setq org-catch-invisible-edits 'smart))

(use-package color-identifiers-mode
  :init (global-color-identifiers-mode 1)
  (diminish 'color-identifiers-mode))

;; (use-package aggressive-indent
;;   :hook ((rustic-mode emacs-lisp-mode) . #'aggressive-indent-mode))

(use-package pip-requirements)

(use-package visual-fill-column)

(use-package olivetti)

(use-package diff-hl
  :config
  (global-diff-hl-mode 1)
  (diff-hl-margin-mode 1)
  (diff-hl-flydiff-mode 1)
  (add-hook 'dired-mode-hook 'diff-hl-dired-mode))

(use-package nov)

;; Use dimmer only if we are in X, on wayland use inactive window transparency
(if (getenv "DISPLAY")
    (use-package dimmer
      :init
      (setq dimmer-fraction 0.35)
      (dimmer-configure-which-key)
      (dimmer-mode t)))

;; (when (getenv "THISISLAPTOP")
;;   (use-package fancy-battery
;;     :config
;;     (defun fancy-battery-default-mode-line ()
;;       "Assemble a mode line string for Fancy Battery Mode.

;; Display the remaining battery time, if available and
;; `fancy-battery-show-percentage' is non-nil, otherwise the
;; percentage.  If the battery is critical, use
;; `battery-critical-face'.  Otherwise use `fancy-battery-charging'
;; or `fancy-battery-discharging', depending on the current state."
;;       (when fancy-battery-last-status
;;         (let* ((time (cdr (assq ?t fancy-battery-last-status)))
;;                (face (pcase (cdr (assq ?b fancy-battery-last-status))
;;                        ("!" 'fancy-battery-critical)
;;                        ("+" 'fancy-battery-charging)
;;                        (_ 'fancy-battery-discharging)))
;;                (percentage (cdr (assq ?p fancy-battery-last-status)))
;;                (status (format " {%s%%%% (%s)}" percentage time)))
;;           (if status
;;               (propertize status 'face face)
;;             ;; Battery status is not available
;;             (propertize "N/A" 'face 'error)))))
;;     (fancy-battery-mode 1)))

(setq mail-user-agent 'gnus-user-agent)
(setq gnus-init-file (custom-lisp-file "gnus-init.el"))

;; (unless noninteractive
;;   (persp-state-load persp-state-default-file))

(use-package vlf)

(use-package org-roam
  :init (setq org-roam-v2-ack t)
  (org-roam-db-autosync-mode)
  :custom (org-roam-directory "~/writes/roam")
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert))
  :config (org-roam-setup)
  (setq org-roam-completion-everywhere t))

(use-package super-save
  :config (setq super-save-auto-save-when-idle t)
  (setq super-save-idle-duration 1)
  (add-to-list 'super-save-triggers 'ace-window)
  (add-to-list 'super-save-hook-triggers 'find-file-hook)
  (setq super-save-max-buffer-size (* 1024 1024 1024))
  (super-save-mode +1)
  (setq auto-save-default nil)
  (diminish 'super-save-mode)
  ;; (mapc (lambda (cmd) (advice-add cmd :before (lambda (&rest _args) (evil-force-normal-state)))) super-save-triggers)
  ;; (mapc (lambda (hook) (add-hook hook #'evil-force-normal-state)) super-save-hook-triggers)
  )


;; (use-package xterm-color
;;   :config
;;   (add-hook 'eshell-before-prompt-hook
;;             (lambda ()
;;               (setq xterm-color-preserve-properties t)))
;;   (add-hook 'eshell-mode-hook (lambda ()
;;                                 (add-to-list 'eshell-preoutput-filter-functions 'xterm-color-filter)))
;;   (setq eshell-output-filter-functions (remove 'eshell-handle-ansi-color eshell-output-filter-functions))
;;   (setenv "TERM" "xterm-256color"))

(use-package eshell-prompt-extras
  :init
  (setq epe-path-style 'full)
  (setq eshell-prompt-function #'epe-theme-multiline-with-status))


(use-package winner
  :init (winner-mode 1))


(use-package with-editor
  :init (add-hook 'shell-mode-hook  'with-editor-export-editor)
  (add-hook 'eshell-mode-hook 'with-editor-export-editor)
  (add-hook 'term-exec-hook   'with-editor-export-editor)
  (add-hook 'vterm-mode-hook  'with-editor-export-editor)
  (add-hook 'after-save-hook
            'executable-make-buffer-file-executable-if-script-p))

(use-package pass)

(use-package avy
  :config
  (setq avy-keys '(?a ?r ?s ?t ?n ?e ?i ?o))
  (setq avy-style 'de-bruijn)
  :bind (("C-," . #'avy-goto-char-timer)
         ("C-:" . avy-goto-line)
         :map isearch-mode-map
         ("C-." . avy-isearch)))

(use-package crontab-mode)

(use-package clojure-mode)
(use-package cider)
(use-package inf-clojure)

(use-package tree-sitter
             :init (global-tree-sitter-mode 1))
(use-package tree-sitter-langs)
(use-package fish-mode)

(use-package nim-mode
  :config (setq nimsuggest-path (expand-file-name "~/.nimble/bin/nimsuggest")))

(use-package go-mode
  :init (add-hook 'go-mode-hook 'lsp-deferred))

(use-package spaceline
  :init
  (setq spaceline-highlight-face-func #'spaceline-highlight-face-evil-state)
  (spaceline-spacemacs-theme))

(use-package lua-mode)

(use-package frames-only-mode
  :init (frames-only-mode +1))

;;; some backup code
(use-package ligature
  :straight (ligature :type git :host github :repo "mickeynp/ligature.el")
  :config
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||"  "<!--" "####" "~~>" "***" "||=" "||>" "::"
                                       ":::" "::=" "=:=" "==="  "=!=" "=<<" "=/=" "!=="
                                       "!!."  ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<=="  "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://"
                                       ;;; "<==>" "==>" "=>>"  ">=>" "<=>" "=>" 
                                       ))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  ;; (global-ligature-mode t)
  )

 ;; '(org-level-1 ((t (:extend nil :foreground "#83a598" :height 1.7))))
 ;; '(org-level-2 ((t (:extend nil :foreground "#fabd2f" :height 1.4))))
 ;; '(org-level-3 ((t (:extend nil :foreground "#d3869b" :height 1.3))))
 ;; '(org-level-4 ((t (:extend nil :foreground "#fb4933" :height 1.2))))
 ;; '(org-level-5 ((t (:extend nil :foreground "#b8bb26" :height 1.1))))
(provide 'init)
;;; init.el ends here
